# Matlabblas-src

Source crate for the BLAS and LAPACK libraries provided with matlab. To be used with the
[BLAS](https://github.com/blas-lapack-rs/blas-lapack-rs.github.io/wiki) crates on crates.io.

## Installation and recommendations
This crate is an empty library; the only think it does is say that it links to another
external library.

Normally, to use BLAS, you would something akin to the following in your `Cargo.toml`
file.
```toml
cblas = "0.4.0"
openblas-src = {version = "0.8", default-features = false, features = ["cblas", "static"], optional = true }
```
As with all the `BLAS` family of crates, you use a binding/wrapper crate to program against
(which pulls in a `sys` crate which provides the actual library symbols), and select a
backend, which provides the implementation (in the above example, that would be
`openblas`).

Instead, iff you are compiling a `mex` file (for example with
[`rustmex`](https://crates.io/crates/rustmex)), and would like to use BLAS, this crate is
for you. Instead of linking to another BLAS backend, you can use this crate to link to
the one Matlab itself [provides](https://nl.mathworks.com/help/matlab/matlab_external/calling-lapack-and-blas-functions-from-mex-files.html)
(and uses). This crate thus takes the place of a `src` crate in `BLAS`'s architecture.

This comes with some extra complexity, however. The onus is on you to provide the correct
paths to `rustc`. Specifically, you have to set the library search path and the library
names to use in your `config.toml` file:
```toml
[target.<triple>.mex]
rustc-link-search = ["~/bin/R2022a/bin/glnxa64"]
rustc-link-lib = ["mwblas", "mwlapack"]
```
Substitute `<triple>` with the triple you are compiling for (the "host" field of the
output of `rustc -vV`). Matlab will print the appropriate search path when you run
`fullfile(matlabroot(), 'bin', computer('arch'))` in your Matlab prompt.
